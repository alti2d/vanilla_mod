extern crate vanilla_mod;

use vanilla_mod::command::*;
use vanilla_mod::event::*;

#[test] fn deserialise_event() {
    let raw = stringify!({
        "port":"27276",
        "time":"0",
        "type":"clientAdd",
        "ip":"0.0.0.0",
        "nickname":"test",
        "player":"0",
        "vaporId":"0",
        "aceRank":"0",
        "level":"40"
    });
    assert_eq!(EventRecord::from_str(raw).unwrap(),
               EventRecord{
                   ev: ServerEvent::ClientAdd{
                       ip: "0.0.0.0".to_owned(),
                       nickname: "test".to_owned(),
                       player: 0,
                       vapor_id: "0".to_owned(),
                       ace_rank: 0,
                       level: 40
                   },
                   port: 27276, time: 0 } );
}

#[test]
fn argument_print() {
    use vanilla_mod::command::Argument;
    assert_eq!(
        "something".print(),
        "something"
    );
    assert_eq!(
        "something with spaces".print(),
        "\"something with spaces\""
    );
    assert_eq!( 5.print(), "5");
}

#[test]
fn command_print() {
    assert_eq!(
        &ServerCommand::Message{
            msg: "Hello World o/".to_owned()}.print(27276),
        "27276,console,serverMessage \"Hello World o/\"");
}
