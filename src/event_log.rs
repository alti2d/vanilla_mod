use std::fs;
use std::io::{BufReader,BufRead};
use event::*;

/// Read new server events from the log file
pub fn read_events(file_path: &str) -> Vec<EventRecord>{
    let mut events = Vec::new();
    match fs::File::open(file_path){
        Ok(file) => {
            let reader = BufReader::new(file);
            let mut lines = reader.lines();
            while let Some(Ok(s)) = lines.next(){
                match EventRecord::from_str(&s){
                    Ok(ev) => events.push(ev),
                    Err(e) => error!("Failed to parse event: {:?}",e)
                }
            }
        }
        Err(e) => warn!("Failed to open log file for reading, {}",e)
    }

    clear_log(file_path);
    events
}

pub fn clear_log(path: &str){
    match fs::File::create(path){
        Ok(file) => file.set_len(0).unwrap(),
        Err(e) => error!("Failed to open log file to clear, {}",e)
    }
}
