use serde_json;
use std::collections::HashMap;

#[derive(Debug,Clone,PartialEq)]
pub struct EventRecord{
    pub ev: ServerEvent,
    pub port: i32,
    pub time: i32,
}

#[derive(Debug,Clone,PartialEq)]
/// An event parsed from the server's JSON log
pub enum ServerEvent{
    Assist{
        player: i32,
        victim: i32,
        xp:     i32,
    },
    BallLost,
    Chat{
        message: String,
        player:  i32,
        server:  bool,
        team:    i32, // 1 if message is a team message
        blocked: bool
    },
    ClientAdd{
        ip:       String,
        nickname: String,
        player:   i32,
        vapor_id: String,
        ace_rank: i32,
        level:    i32
    },
    ClientNicknameChange{
        ip:           String,
        old_nickname: String,
        new_nickname: String,
        player:       i32,
        vapor_id:     String,
    },
    ClientRemove{
        ip:       String,
        nickname: String,
        player:   i32,
        reason:   String,
        vapor_id: String,
        message:  String
    },
    ConsoleCommandExecute{
       arguments: Vec<String>,
       command:   String,
       group:     String,
       source:    String,
    },
    CustomCommand{
       command: String,
    },
    DemolitionChargeDefused{
       player: i32,
    },
    DemolitionChargePlanted{
       player: i32,
    },
    Goal{
       assister:      i32,
       second_assist: i32,
       player:        i32,
       xp:            i32,
    },
    Kill{
       multi:        i32,
       player:       i32,
       source:       String,
       streak:       i32,
       victim:       i32,
       xp:           i32,
       victim_pos_x: f32,
       victim_pos_y: f32,
       victim_vel_x: f32,
       victim_vel_y: f32,
    },
    LogPlanePositions{
        time: i32,
        positions: HashMap<i32,String>,
    },
    LogServerStatus{
        nicknames:  Vec<String>,
        ips:        Vec<String>,
        vapor_ids:  Vec<String>,
        player_ids: Vec<i32>,
        tournament: bool,
    },
    MapChange{
       map:  String,
       mode: String,
    },
    MapLoading{
       map: String,
    },
    ObjectiveStart{
        time:       i32,
        right_team: i32,
        left_team:  i32,
    },
    ObjectiveEnd{
        time:         i32,
        winning_team: i32,
        length:       i32,
    },
    PingSummary{
        time:           i32,
        ping_by_player: HashMap<String,i32>,
    },
    PowerupAutoUse{
       player:     i32,
       position_x: f32,
       position_y: f32,
       powerup:    String,
    },
    PowerupDefuse{
       player:     i32,
       position_x: f32,
       position_y: f32,
       powerup:    String,
       xp:         i32,
    },
    PowerupPickup{
       player:     i32,
       position_x: f32,
       position_y: f32,
       powerup:    String,
    },
    PowerupUse{
       player:     i32,
       position_x: f32,
       position_y: f32,
       powerup:    String,
       velocity_x: f32,
       velocity_y: f32,
    },
    ServerHitch{
       changed_map: bool,
       duration:    i32
    },
    ServerInit{
       max_player_count: i32,
       name:             String
    },
    ServerShutdown,
    ServerStart,
    SessionStart{
       date: String
    },
    Spawn{
       perk_blue:  String,
       perk_green: String,
       perk_red:   String,
       plane:      String,
       player:     i32,
       skin:       String,
       team:       i32
    },
    StructureDamage{
       player: i32,
       target: String,
       xp:     i32,
    },
    StructureDestroy{
       player: i32,
       target: String,
       xp:     i32,
    },
    TeamChange{
       player: i32,
       team:   i32
    },
    TournamentRoundEnd{
       losers:  Vec<String>,
       result:  i32,
       winners: Vec<String>,
    },
    TournamentStart{
       team0: Vec<String>,
       team1: Vec<String>
    },
    TournamentStop,
    UpdatePrepareRestart,
}

#[derive(Debug)]
pub enum Error{
    MissingParam(String),
    UnknownEvent(String),
    JsonError(serde_json::Error, String),
}

macro_rules! parse_ev {
    ($ev:ident $t:ident; $($name:ident),*; $($json_name:ident $name2:ident),*) => {
        Ok(ServerEvent::$t{
            $($name: match $ev.$name{
                Some(x) => {x},
                None => {return Err(Error::MissingParam(stringify!($name).to_owned()));}
            }),*,
            $($name2: match $ev.$json_name{
                Some(x) => {x}
                None => {return Err(Error::MissingParam(stringify!($json_name).to_owned()));}
            }),*
        })
    }
}

impl EventRecord{
    /// Create a server event from the given JSON str, returns the server
    /// event, the port of the server, and the time.
    pub fn from_str(s: &str) -> Result<EventRecord,Error>{
        // The team property can be either an integer or a bool,
        // let's change that.
        let s = s.to_owned()
            .replace("\"team\":true", "\"team\":\"1\"")
            .replace("\"team\":false", "\"team\":\"0\"");

        let raw = match serde_json::from_str::<ServerEventRaw>(&s){
            Ok(x) => x,
            Err(e) => return Err(Error::JsonError(e,s.to_owned())),
        };
        let port = raw.port;
        let time = raw.time.expect("Event has no time property!");
        Ok(EventRecord{
            ev: try!(ServerEvent::from_raw(raw)),
            port: port,
            time: time
        })
    }
}

impl ServerEvent{
    fn from_raw(ev: ServerEventRaw) -> Result<ServerEvent,Error>{
        match &ev.event_type[..]{
            "assist" => parse_ev!(ev Assist;
               player, victim, xp;),
            "ballLost" => Ok(ServerEvent::BallLost),
            "chat" => parse_ev!(ev Chat;
               blocked, team, message, player, server;),
            "clientAdd" => parse_ev!(ev ClientAdd;
                 ip, nickname, player, level; vaporId vapor_id, aceRank ace_rank),
            "clientNicknameChange" => parse_ev!(ev ClientNicknameChange;
               ip, player; newNickname new_nickname, oldNickname old_nickname, vaporId vapor_id),
            "clientRemove" => parse_ev!(ev ClientRemove;
               ip, nickname, player, reason, message; vaporId vapor_id),
            "consoleCommandExecute" => parse_ev!(ev ConsoleCommandExecute;
               arguments, command, group, source;),
            "customCommand" => parse_ev!(ev CustomCommand;
               command;),
            "demolitionChargeDefused" => parse_ev!(ev DemolitionChargeDefused;
               player;),
            "demolitionChargePlanted" => parse_ev!(ev DemolitionChargePlanted;
               player;),
            "goal" => parse_ev!(ev Goal;
               assister, player, xp; secondaryAssister second_assist),
            "kill" => parse_ev!(ev Kill;
               multi, player, source, streak, victim, xp;
               victimPositionX victim_pos_x, victimPositionY victim_pos_y,
               victimVelocityX victim_vel_x, victimVelocityY victim_vel_y),
            "logPlanePositions" => parse_ev!(ev LogPlanePositions;
               time ; positionByPlayer positions),
            "logServerStatus" => parse_ev!(ev LogServerStatus;
               nicknames, ips; vaporIds vapor_ids, tournamentInProgress tournament,
               playerIds player_ids),
            "mapChange" => parse_ev!(ev MapChange;
               map, mode;),
            "mapLoading" => parse_ev!(ev MapLoading;
               map;),
            "objectiveGameStart" => parse_ev!(ev ObjectiveStart;
               time; rightTeam right_team, leftTeam left_team),
            "objectiveGameEnd" => parse_ev!(ev ObjectiveEnd;
               time; timeSeconds length, winningTeam winning_team),
            "pingSummary" => parse_ev!(ev PingSummary; time; pingByPlayer ping_by_player),
            "powerupAutoUse" => parse_ev!(ev PowerupAutoUse;
               player, powerup; positionX position_x, positionY position_y),
            "powerupDefuse" => parse_ev!(ev PowerupDefuse;
               player, powerup, xp; positionX position_x, positionY position_y),
            "powerupPickup" => parse_ev!(ev PowerupPickup;
               player, powerup; positionX position_x, positionY position_y),
            "powerupUse" => parse_ev!(ev PowerupUse;
               player, powerup; positionX position_x, positionY position_y,
               velocityX velocity_x, velocityY velocity_y),
            "serverHitch" => parse_ev!(ev ServerHitch;
               duration; changedMap changed_map),
            "serverInit" => parse_ev!(ev ServerInit;
               name; maxPlayerCount max_player_count),
            "serverShutdown" => Ok(ServerEvent::ServerShutdown),
            "serverStart" => Ok(ServerEvent::ServerStart),
            "sessionStart" => parse_ev!(ev SessionStart; date;),
            "spawn" => parse_ev!(ev Spawn;
               plane, player, skin, team; perkRed perk_red, perkBlue perk_blue,
               perkGreen perk_green),
            "structureDamage" => parse_ev!(ev StructureDamage;
               player, target, xp;),
            "structureDestroy" => parse_ev!(ev StructureDestroy;
               player, target, xp;),
            "teamChange" => parse_ev!(ev TeamChange;
               player, team;),
            "tournamentRoundEnd" => parse_ev!(ev TournamentRoundEnd;
               losers, result, winners;),
            "tournamentStart" => parse_ev!(ev TournamentStart; team0, team1;),
            "tournamentStop" => Ok(ServerEvent::TournamentStop),
            "updatePrepareRestart" => Ok(ServerEvent::UpdatePrepareRestart),

            x => return Err(Error::UnknownEvent(x.to_owned())),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[allow(non_snake_case)]
pub struct ServerEventRaw{
    #[serde(rename="type")]
    event_type:             String,
    port:                   i32,
    time:                   Option<i32>, // bah

    aceRank:                Option<i32>,
    arguments:              Option<Vec<String>>,
    assister:               Option<i32>,
    blocked:                Option<bool>,
    changedMap:             Option<bool>,
    command:                Option<String>,
    date:                   Option<String>,
    demo:                   Option<bool>,
    duration:               Option<i32>,
    exactXp:                Option<i32>,
    group:                  Option<String>,
    ip:                     Option<String>,
    ips:                    Option<Vec<String>>,
    leftTeam:               Option<i32>,
    level:                  Option<i32>,
    losers:                 Option<Vec<String>>,
    map:                    Option<String>,
    maxPlayerCount:         Option<i32>,
    message:                Option<String>,
    mode:                   Option<String>,
    multi:                  Option<i32>,
    name:                   Option<String>,
    newNickname:            Option<String>,
    nickname:               Option<String>,
    nicknames:              Option<Vec<String>>,
    oldNickname:            Option<String>,
    participantStatsByName: Option<HashMap<String,Vec<i32>>>,
    participants:           Option<Vec<i32>>,
    perkBlue:               Option<String>,
    perkGreen:              Option<String>,
    perkRed:                Option<String>,
    pingByPlayer:           Option<HashMap<String,i32>>,
    plane:                  Option<String>,
    player:                 Option<i32>,
    playerIds:              Option<Vec<i32>>,
    positionByPlayer:       Option<HashMap<i32,String>>,
    positionX:              Option<f32>,
    positionY:              Option<f32>,
    powerup:                Option<String>,
    reason:                 Option<String>,
    result:                 Option<i32>,
    rightTeam:              Option<i32>,
    secondaryAssister:      Option<i32>,
    server:                 Option<bool>,
    skin:                   Option<String>,
    source:                 Option<String>,
    streak:                 Option<i32>,
    target:                 Option<String>,
    team0:                  Option<Vec<String>>,
    team1:                  Option<Vec<String>>,
    team:                   Option<i32>,
    timeSeconds:            Option<i32>,
    tournamentInProgress:   Option<bool>,
    vaporId:                Option<String>,
    vaporIds:               Option<Vec<String>>,
    velocityX:              Option<f32>,
    velocityY:              Option<f32>,
    victim:                 Option<i32>,
    victimPositionX:        Option<f32>,
    victimPositionY:        Option<f32>,
    victimVelocityX:        Option<f32>,
    victimVelocityY:        Option<f32>,
    winnerByAward:          Option<HashMap<String,i32>>,
    winners:                Option<Vec<String>>,
    winningTeam:            Option<i32>,
    xp:                     Option<i32>,
}
