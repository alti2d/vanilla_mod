use std::{io,fs};

/// A single argument to a server command
pub trait Argument{
    fn print(&self) -> String;
}

/// `ServerCommand` represents a console command for a particular server. It can
/// be written as a command.txt entry through its `print` method.
#[derive(Debug,Clone)]
pub enum ServerCommand {
    AssignTeam { nick: String, team: i32 },
    ModifyTournament { nick: String, team: i32 },
    Ban { nick: String, length: i32, unit: String, reason: String },
    GetStatus,
    GravityMode { v: i32 },
    HealthMod { v: i32 },
    Map { map: String },
    Message { msg: String },
    PlaneScale { v: i32 },
    StartTournament,
    StopTournament,
    ViewScale { v: i32 },
    WeaponMode { v: i32 },
    Whisper { nick: String, msg: String },
}

#[allow(unused_mut)]
impl ServerCommand {
    /// `print` prints a `ServerCommand` into a form that can be interpreted
    /// by the altitude server. It handles all escaping and special cases.
    pub fn print(&self, p: i32) -> String {
        use self::ServerCommand::*;

        macro_rules! commands {
            ($this:expr;
             $($port:expr, $name:ident, $command:expr; {$($arg:ident)*})+) => {
                match $this{
                    $($name { $(ref $arg),* } =>{
                        let mut s = format!("{},console,{} ",$port,$command);
                        $(s.push_str(&$arg.print()); s.push(' ');)*
                        s.trim().to_owned()
                    })*
                }
            }
        }

        commands! { *self;
            p, AssignTeam, "assignTeam"; {nick team}
            p, ModifyTournament, "modifyTournament"; {nick team}
            p, Ban, "ban"; {nick length unit reason}
            p, GetStatus, "logServerStatus"; {}
            p, GravityMode, "testGravityMode"; {v}
            p, HealthMod, "testHealthModifier"; {v}
            p, Map, "changeMap"; {map}
            p, Message, "serverMessage"; {msg}
            p, PlaneScale, "testPlaneScale"; {v}
            p, StartTournament, "startTournament"; {}
            p, StopTournament, "stopTournament"; {}
            p, ViewScale, "testCameraViewScale"; {v}
            p, WeaponMode, "testDisableWeaponMode"; {v}
            p, Whisper, "serverWhisper"; {nick msg}
        }
    }
}

/// Write a slice of `ServerCommand`s to the specified `command.txt` file.
pub fn write_cmds(path: &str, cmds: &[(ServerCommand,i32)])
                  -> io::Result<()>{
    use std::io::Write;
    let mut file = try!(fs::OpenOptions::new().append(true).open(path));

    for &(ref c,port) in cmds{
        debug!("writing command {:?}", c);
        try!(write!(file, "{}\n", c.print(port)));
    }
    Ok(())
}

impl<'a> Argument for &'a str{
    fn print(&self) -> String {
        if self.contains(" ") || self.contains("\""){
            format!("{:?}",self)
        } else {
            format!("{}",self)
        }
    }
}
impl Argument for String{
    fn print(&self) -> String { (&self[..]).print() }
}
impl Argument for i32{
    fn print(&self) -> String { format!("{}",self) }
}
