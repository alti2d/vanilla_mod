#![feature(custom_derive, plugin)]
#![plugin(serde_macros)]

#[macro_use] extern crate log;
extern crate serde;
extern crate serde_json;

pub mod command;
pub mod event;
pub mod event_log;
pub mod player;
pub mod whitelist;

use command::*;
use event_log::*;
use event::*;
use player::*;
use std::thread;
use std::time::Duration;
use std::collections::{HashMap,HashSet};

pub struct EventLoop{
    pub tick:     Duration,
    pub log_path: &'static str,
    pub cmd_path: &'static str,
    players:      HashMap<i32,PlayerMap>,
    cmd_out:       Vec<(ServerCommand,i32)>,
}

pub trait Handler{
    fn init(&mut self, evl: &mut EventLoop);
    fn handle(&mut self, ev: EventRecord, evl: &mut EventLoop);
    fn tick(&mut self, evl: &mut EventLoop);
}

impl EventLoop{
    pub fn new() -> EventLoop{
        EventLoop{
            tick:     Duration::from_millis(50),
            log_path: "servers/log.txt",
            cmd_path: "servers/command.txt",
            players:  HashMap::new(),
            cmd_out:   Vec::new(),
        }
    }

    pub fn cmd(&mut self, e: ServerCommand, port: i32){
        self.cmd_out.push((e,port));
    }

    pub fn cmds(&mut self, e: &[ServerCommand], port: i32){
        for cmd in e { self.cmd_out.push((cmd.clone(),port)); }
    }

    pub fn players(&mut self, port: i32) -> &mut PlayerMap{
        if let None = self.players.get_mut(&port){
            self.players.insert(port, PlayerMap::new());
        }
        self.players.get_mut(&port).unwrap()
    }

    pub fn run(mut self, mut handlers: HashMap<i32, Vec<Box<Handler>>>){
        clear_log(self.log_path);

        for (port,_) in &mut handlers{
            if *port > 0 {
                write_cmds(self.cmd_path, &[(ServerCommand::GetStatus,*port)])
                    .expect("Failed to write status commands to file");
            }
        }

        let mut init = HashSet::new();

        loop{
            let events = read_events(self.log_path);

            for (_,h) in &mut handlers{
                for x in h { x.tick(&mut self); }
            }

            for e in events{
                update_players(self.players(e.port), &e.ev);

                if let Some(handlers) = handlers.get_mut(&e.port){
                    let mut to_init = false;
                    let mut handle = true;
                    if !init.contains(&e.port){
                        handle = false;
                        if let ServerEvent::LogServerStatus{ .. } = e.ev{
                            to_init = true;
                            init.insert(e.port);
                        }
                    }
                    if to_init { for h in handlers.iter_mut(){
                        h.init(&mut self);
                    }}
                    if handle { for h in handlers.iter_mut(){
                        h.handle(e.clone(), &mut self);
                    }}
                }

                write_cmds(self.cmd_path, &self.cmd_out[..])
                    .expect("Failed to write commands to file");
                self.cmd_out.clear();
            }
            thread::sleep(self.tick);
        }
    }
}
