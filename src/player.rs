use std::str::FromStr;
use std::collections::HashMap;
use serde;
use serde_json::Value;
use event::*;

pub type PlayerMap = HashMap<i32, Player>;

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub enum PermissionLevel{
    None = 0, AA, Member, Admin, Leader
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Player{
    pub ip:         String,
    pub vapor_id:   String,
    pub nickname:   String,
    pub permission: PermissionLevel,
}

pub fn update_players(players: &mut PlayerMap, event: &ServerEvent){
    use event::ServerEvent::*;
    match *event{
        ClientAdd{ ref ip, ref nickname, ref player, ref vapor_id, .. } => {
            let new_player = Player{
                ip:         ip.clone(),
                vapor_id:   vapor_id.clone(),
                nickname:   nickname.clone(),
                permission: PermissionLevel::None };
            debug!("Adding player {:?}",new_player);
            players.insert(*player, new_player);
        }
        ClientRemove{ ref player, .. } => {
            debug!("Removing player {:?}", players.remove(player));
        }
        ClientNicknameChange{ ref player, ref new_nickname, .. } => {
            players.get_mut(player).unwrap().nickname = new_nickname.clone();
        }
        LogServerStatus{ref nicknames,ref ips,ref vapor_ids, ref player_ids, ..} => {
            players.clear();
            for i in 0..nicknames.len(){
                players.insert(player_ids[i], Player{
                    ip: ips[i].clone(), vapor_id: vapor_ids[i].clone(),
                    nickname: nicknames[i].clone(),
                    permission: PermissionLevel::None });
            }
        }
        _ => ()
    }
}

pub fn find_vapor_id(m: &PlayerMap, vid: &str) -> Option<i32>{
    for p in m{
        if p.1.vapor_id == vid { return Some(*p.0) }
    }
    None
}
pub fn find_nickname(m: &PlayerMap, nick: &str) -> Option<i32>{
    for p in m{
        if p.1.nickname == nick { return Some(*p.0) }
    }
    None
}

impl serde::Serialize for PermissionLevel {
    fn serialize<S>(&self, ser: &mut S) -> Result<(), S::Error>
    where S: serde::Serializer, {
        ser.serialize_str(&format!("{:?}",*self))
    }
}
impl serde::Deserialize for PermissionLevel {
    fn deserialize<D>(de: &mut D) -> Result<Self, D::Error>
        where D: serde::Deserializer{
        let res = try!(serde::Deserialize::deserialize(de));
        match res {
            Value::String(ref s) => PermissionLevel::from_str(s)
                .map_err(|e| serde::de::Error::custom(e)),
            _ => Err(serde::de::Error::custom("Unexpected value")),
        }
    }
}

impl FromStr for PermissionLevel{
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err>{
        match &s.to_lowercase()[..]{
            "none" => Ok(PermissionLevel::None),
            "aa" => Ok(PermissionLevel::AA),
            "member" => Ok(PermissionLevel::Member),
            "admin" => Ok(PermissionLevel::Admin),
            "leader" => Ok(PermissionLevel::Leader),
            x => Err(format!("Unknown permission level: {}",x)),
        }
    }
}
