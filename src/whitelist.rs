//! A simple server whitelist. Requires the corresponding
//! game.jar mod.

use std::fs;
use std::io::{self,Write};

/// Enable the whitelist for the server on the given port. Only players with
/// the given vapor IDs will be allowed to spawn.
pub fn enable(port: i32, vapor_ids: &[String]) -> io::Result<()>{
    let _ = fs::remove_file(&format!("allow_all_{}",port));
    let mut whitelist = try!(fs::File::create(&format!("whitelist_{}",port)));
    for v in vapor_ids{
        try!(writeln!(whitelist, "{}", v));
    }
    Ok(())
}

/// Disable the whitelist for the server on the give port,
/// allowing all players to spawn.
pub fn disable(port: i32) -> io::Result<()>{
    let mut allow = try!(fs::File::create(format!("allow_all_{}",port)));
    writeln!(allow,"")
}
